# osu-sr-calculator
Package to calculate star ratings with any mod combination on osu! beatmaps
Using the recently updated star rating algorithm

## Installation
```sh
npm install osu-sr-calculator --save
```
```sh
yarn add osu-sr-calculator
```

## Usage
```typescript
import { calculateStarRating } from 'osu-sr-calculator';

const starRating = await calculateStarRating(mapId, mods, allCombinations);
```

The calculateStarRating function accepts three parameters:
* mapId: the map id of the beatmap (make sure you use the id from the /b url)
* mods (optional): an array containing the mods you want to include in the calculation. Omitting this parameter will default to nomod. Any mod combination can be used, but only HR, EZ, DT and HT will influence the star rating. Any invalid string will be ignored.
* allCombinations (optional): a boolean containing whether or not to calculate all possible star ratings for this map. If set to true, the mods parameter will obviously be ignored.

This function returns an object containing the calculated star rating for the requested mod combination. In case of allCombinations, the object will contain all mod combinations with their corresponding star ratings. 

## Examples

* Nomod star rating:
```typescript
const starRating = await calculateStarRating(1616712);
```

* DT star rating:
```typescript
const starRating = await calculateStarRating(1616712, ["DT"]);
```

* DTHR star rating:
```typescript
const starRating = await calculateStarRating(1616712, ["DT", "HR"]);
```

* All possible star ratings:
```typescript
const starRating = await calculateStarRating(1616712, [], true);
```

## Accuracy
This package parses the beatmap and calculates the star rating in exactly the same way as in the official osu! source code.
Due to slight difference between C# and TypeScript, a fault margin of at most 0.01 should be expected.
Accuracy on "unrankable" beatmaps can be further off.