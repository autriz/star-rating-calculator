import { OsuService } from "./osu-service";
import BeatmapParser from "./BeatmapParser";
import DifficultyHitObjectCreator from "./DifficultyHitObjectCreator";
import StarRatingCalculator from "./StarRatingCalculator";

const osuService = new OsuService();
const beatmapParser = new BeatmapParser();
const difficultyHitObjectCreator = new DifficultyHitObjectCreator();
const starRatingCalculator = new StarRatingCalculator();
let Beatmap = null;

export async function calculateStarRating(map_id: number, mods?: string[], allCombinations?: boolean): Promise<object> {
    const map = await getOsuBeatmap(map_id);
    if (map === null)
        throw new Error("No map found for specified map id");

    mods = parseMods(mods);
    let output = { };
    if (!allCombinations) {
        const label = mods.length > 0 ? mods.join('') : "nomod";
        output[label] = calculateNextModCombination(map, mods, true);
        return output;
    }
    else {
        const allModCombinations = getAllModCombinations();
        allModCombinations.forEach(combi => {
            const label = combi.mods.length > 0 ? combi.mods.join('') : "nomod";
            output[label] = calculateNextModCombination(map, combi.mods, combi.reParse);
        });
        return output;
    }
}

function calculateNextModCombination(map: string, mods: string[], reParse: boolean): number {
    if (reParse)
        Beatmap = beatmapParser.parseBeatmap(map, mods);

    const timeRate = getTimeRate(mods);
    const difficultyHitObjects = difficultyHitObjectCreator.convertToDifficultyHitObjects(Beatmap.HitObjects, timeRate);
    return starRatingCalculator.calculate(difficultyHitObjects, timeRate);
};

async function getOsuBeatmap(map_id: number): Promise<string> {
    return await osuService.getOsuBeatmap(map_id);
};

function parseMods(mods: string[]): string[] {
    if (mods === undefined)
        return [];
    return mods;
};

function getTimeRate(mods: string[]): number {
    if (mods.includes("DT"))
        return 1.5;
    if (mods.includes("HT"))
        return 0.75;
    return 1;
};

function getAllModCombinations(): Array<{ mods: string[], reParse: boolean }> {
    return [
        { mods: [], reParse: true },
        { mods: ["DT"], reParse: false },
        { mods: ["HT"], reParse: false },
        { mods: ["HR"], reParse: true },
        { mods: ["HR", "DT"], reParse: false },
        { mods: ["HR", "HT"], reParse: false },
        { mods: ["EZ"], reParse: true },
        { mods: ["EZ", "DT"], reParse: false },
        { mods: ["EZ", "HT"], reParse: false }
    ];
};